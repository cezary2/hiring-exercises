# frozen_string_literal: true

class Config
  attr_reader :year, :team_name

  def initialize(args)
    # NOTE: we could use optparse for maintainability, but it's overkill given the requirements

    @year = :all
    @team_name = :all
    case args.size
    when 0
    when 1
      arg = args[0]
      if is_year?(arg)
        @year = Integer(arg)
      else
        @team_name = arg
      end
    when 2
      arg1, arg2 = args
      if is_year?(arg1)
        @year = Integer(arg1)
        @team_name = arg2
      elsif is_year?(arg2)
        @year = Integer(arg2)
        @team_name = arg1
      else
        Kernel.abort "Neither #{arg1.inspect} nor #{arg2.inspect} is a valid year"
      end
    else
      Kernel.abort "Usage: #{$PROGRAM_NAME} [<year> | <team>]"
    end
  end

  # NOTE: we could use accessors or consts for these, but methods are easier to extend

  def batting_datafile
    'Batting.csv'
  end

  def teams_datafile
    'Teams.csv'
  end

  def report_columns
    ['playerID', 'yearId', 'Team name(s)', 'Batting Average']
  end

  private

  def is_year?(arg)
    arg =~ /\d{4}/
  end
end
