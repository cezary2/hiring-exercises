# frozen_string_literal: true

require_relative '../config'
require_relative '../db'
require_relative '../report'

class TestConfig < Config
  def batting_datafile
    'spec/fixtures/multiple_stints.csv'
  end

  def teams_datafile
    'spec/fixtures/multiple_stints_teams.csv'
  end
end

class NoBattingConfig < Config
  def batting_datafile
    'spec/fixtures/no_batting.csv'
  end

  def teams_datafile
    'spec/fixtures/multiple_stints_teams.csv'
  end
end

RSpec.describe 'Batting Average Data' do
  let(:db) { DB.new(config) }

  before do
    allow(Kernel).to receive(:abort).and_raise(NotImplementedError, ':abort is not stubbed')
  end

  context 'with no batting approaches' do
    let(:config) { NoBattingConfig.new([]) }
    it "skips players that don't rank" do
      actual = Report.new(config, db).table_data
      expect(actual).to eq([])
    end
  end

  context 'when filtered by year only' do
    let(:config) { TestConfig.new(['1891']) }
    context 'with multiple matching stints' do
      it 'shows accumulated batting averages' do
        actual = Report.new(config, db).table_data
        expected_average = ((59 + 74.0) / (216 + 203)).round(3).to_s

        expected =
          { 'Batting Average' => expected_average,
            'Team name(s)' => 'Cincinnati Reds, Pittsburgh Pirates', 'playerID' => 'brownpe01', 'yearId' => 1891 }

        expect(actual).to eq([expected])
      end
    end
  end

  context 'when filtered by year and team_name' do
    let(:config) { TestConfig.new(['1891', 'Cincinnati Reds']) }
    it 'shows accumulated batting averages' do
      actual = Report.new(config, db).table_data
      expected_average = (74.0 / 216).round(3).to_s

      expected =
        { 'Batting Average' => expected_average,
          'Team name(s)' => 'Cincinnati Reds', 'playerID' => 'brownpe01', 'yearId' => 1891 }

      expect(actual).to eq([expected])
    end
  end

  context 'when filtered by team_name and year' do
    let(:config) { TestConfig.new(['Cincinnati Reds', '1891']) }
    it 'shows accumulated batting averages' do
      actual = Report.new(config, db).table_data
      expected_average = (74.0 / 216).round(3).to_s

      expected =
        { 'Batting Average' => expected_average,
          'Team name(s)' => 'Cincinnati Reds', 'playerID' => 'brownpe01', 'yearId' => 1891 }

      expect(actual).to eq([expected])
    end
  end

  context 'when filtered by team_name' do
    let(:config) { TestConfig.new(['Cincinnati Reds']) }
    it 'shows accumulated batting averages for matching stint' do
      actual = Report.new(config, db).table_data
      expected_average = (74.0 / 216).round(3).to_s

      expected =
        { 'Batting Average' => expected_average,
          'Team name(s)' => 'Cincinnati Reds', 'playerID' => 'brownpe01', 'yearId' => 1891 }

      expect(actual).to eq([expected])
    end
  end

  context 'when not filtered' do
    let(:config) { TestConfig.new([]) }
    context 'with multiple matching stints' do
      it 'shows sorted accumulated batting averages' do
        actual = Report.new(config, db).table_data

        expected = [
          { 'Batting Average' => '0.317', # (59+74)/(216+203)
            'Team name(s)' => 'Cincinnati Reds, Pittsburgh Pirates',
            'playerID' => 'brownpe01',
            'yearId' => 1891 },
          { 'Batting Average' => '0.273', # 155/567
            'Team name(s)' => 'Tampa Bay Rays',
            'playerID' => 'phamth01',
            'yearId' => 2019 },
          { 'Batting Average' => '0.250', # 9/36
            'Team name(s)' => 'Chicago Cubs',
            'playerID' => 'zagunma01',
            'yearId' => 2019 }
        ]

        expect(actual).to eq(expected)
      end
    end
  end
end

RSpec.describe 'Batting Average Report' do
  let(:db) { DB.new(config) }

  before do
    allow(Kernel).to receive(:abort).and_raise(NotImplementedError, ':abort is not stubbed')
    allow($stdout).to receive(:tty?).and_return(false)
  end

  let(:config) { TestConfig.new([]) }
  context 'with multiple matching stints' do
    it 'shows formatted table' do
      expected = <<EOF
  +-----------+--------+-------------------------------------+-----------------+
  | playerID  | yearId | Team name(s)                        | Batting Average |
  +-----------+--------+-------------------------------------+-----------------+
  | brownpe01 | 1891   | Cincinnati Reds, Pittsburgh Pirates | 0.317           |
  | phamth01  | 2019   | Tampa Bay Rays                      | 0.273           |
  | zagunma01 | 2019   | Chicago Cubs                        | 0.250           |
  +-----------+--------+-------------------------------------+-----------------+
EOF

      expect do
        Report.new(config, db).report
      end.to output(expected).to_stdout
    end
  end
end
