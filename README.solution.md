# Solution to hiring exercise.

## Approach

This approach (on this branch) uses Ruby directly.

### Pros:

- more flexible (without having to carefully tweak otherwise complex, DB-specific queries)
- less db-related dependencies and no database to configure to run the exammple

### Cons:

- verbose
- slow (doing lots of work that DB would do much faster)
- harder to maintain
- no persistence between runs (slow every time)
- easier to make certain errors (mitigated by tests)


## Main points:

- around 97% test coverage
- team names depend on year (which isn't mentioned in the original README.backend.md)
- formatador is used to generate html table
- tests are mostly end-to-end


## Issues:

- not production code (it's an exercise), so no point in Rubocop setup, etc.
- needs better decoupling of classes (single responsibility)
- business needs weren't defined (making technical decisions arbitrary)
- some edge cases aren't properly defined
- possible to speed up the code
