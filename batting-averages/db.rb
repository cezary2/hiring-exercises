# frozen_string_literal: true

require 'csv'
require 'set'

class DB
  attr_reader :config

  def initialize(config)
    @config = config
  end

  def raw_teams
    @raw_teams ||=
      read_raw(config.teams_datafile, converters: [
                 lambda do |value, field_info|
                   case field_info.header
                   when 'name'
                     value.to_s
                   when 'teamID'
                     value.to_s
                   when 'yearID'
                     Integer(value)
                   end
                 end
               ])
  end

  def raw_batting
    @raw_batting ||= read_raw(config.batting_datafile, converters: [
                                lambda do |value, field_info|
                                  case field_info.header
                                  when 'playerID'
                                    value.to_s
                                  when 'yearID'
                                    Integer(value)
                                  when 'stint'
                                    Integer(value)
                                  when 'teamID'
                                    value.to_s
                                  when 'AB'
                                    Integer(value)
                                  when 'H'
                                    Integer(value)
                                  end
                                end
                              ])
  end

  def read_raw(filename, options = {})
    CSV.read(filename, **{ headers: true }.merge(options))
  end

  def filtered_team_ids
    # TODO: break sooner if one team given
    @filtered_team_ids ||=
      begin
        year = config.year
        team_name = config.team_name

        ids = raw_teams.select do |row|
          (team_name == :all || (row.fetch('name') == team_name)) &&
            (year == :all || (row.fetch('yearID') == year))
        end.map do |row|
          row.fetch('teamID')
        end

        Set.new(ids)
      end
  end

  def include_row?(row)
    year = config.year
    team_name = config.team_name

    return false unless row.fetch('AB').positive?

    # NOTE: optimization
    return true if year == :all && team_name == :all
    return false if year != :all && row.fetch('yearID') != year
    return true if team_name == :all

    filtered_team_ids.include?(row.fetch('teamID'))
  end

  def team_names
    # TODO: break sooner if filtering for one team given
    @team_names ||=
      begin
        result = raw_teams.map do |row|
          key = [row['yearID'], row['teamID']]
          [key, row['name']]
        end
        Hash[result]
      end
  end
end
