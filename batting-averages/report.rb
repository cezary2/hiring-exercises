# frozen_string_literal: true

require 'formatador'
require 'ostruct'

require_relative 'config'
require_relative 'db'

class BatterYearInfo
  attr_reader :player_id, :year, :team_names, :batting_average

  def initialize(name, year, data, team_name_map)
    @player_id = name
    @year = year

    stint = data.values

    @team_names = stint.map(&:team_id).map do |team_id|
      team_name_map.fetch([year, team_id])
    end.sort.uniq.join(', ')

    # TODO: filter out records without an average?
    at_bats = stint.map(&:at_bats).sum
    hits = Float(stint.map(&:hits).sum)
    @batting_average = at_bats > 0.0 ? hits / at_bats : nil
  end

  def to_hash
    {
      'playerID' => player_id,
      'yearId' => year,
      'Team name(s)' => team_names,
      'Batting Average' => format('%.3f', batting_average.round(3))
    }
  end
end

class Report
  attr_reader :config, :db

  def initialize(config, db)
    @config = config
    @db = db
  end

  def batter_year_info
    @batter_year_info ||=
      begin
        {}.tap do |batter_years|
          db.raw_batting.select do |row|
            db.include_row?(row)
          end.each do |row|
            row = OpenStruct.new(row.to_hash)
            id = row.playerID
            year = row.yearID
            stint_number = row.stint
            team_id = row.teamID
            at_bats = row.AB
            hits = row.H

            stint = OpenStruct.new(team_id: team_id, at_bats: at_bats, hits: hits)
            key = [id, year]
            batter_year = (batter_years[key] ||= {})

            raise NotImplementedError("Stint number already exists: #{stint_number}") if batter_year.key?(stint_number)

            batter_year[stint_number] = stint
          end
        end
      end
  end

  def table_data
    batter_year_info.map do |name_and_year, data|
      BatterYearInfo.new(*name_and_year, data, db.team_names)
    end.sort_by(&:batting_average).reverse.map(&:to_hash)
  end

  def report
    # NOTE: colorized (not in requirements) and compact, but those are either features or easy to change
    Formatador.display_compact_table(table_data, config.report_columns)
  end
end

# :nocov:
if $PROGRAM_NAME == __FILE__
  config = Config.new(ARGV)
  db = DB.new(config)
  Report.new(config, db).report
end
# :nocov:
